# utils: Python utilities used in various scripts
# Copyright (C) 2023  Charles H. Pence
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
from pathlib import Path

import requests
from tqdm import tqdm

from utils.net.get_retry import get_retry


def download_file(
    url: str, target: str | Path, desc: str = "Downloading"
) -> None:
    """
    Download a file to a target location on disk. This function will
    automatically determine the filesize and display a formatted progress bar
    with tqdm.

    :param url: URL of the file to download.
    :param target: File path at which to download the file.
    :param desc: Optional description of the download for the progress bar.
    """
    response = get_retry(url, stream=True)
    if response is None:
        return

    # Try to get complete file size in advance
    total = response.headers.get("Content-Length")
    if total is not None:
        total = int(total)

    # Do the download with a nice progress bar
    with tqdm(
        desc=desc, unit="B", unit_scale=True, unit_divisor=1024, total=total
    ) as bar:
        with open(target, "wb") as handle:
            for data in response.iter_content():
                handle.write(data)
                bar.update(len(data))
