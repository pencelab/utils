# utils: Python utilities used in various scripts
# Copyright (C) 2023  Charles H. Pence
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
from http import HTTPStatus
from time import sleep
from typing import Any, Callable

import requests


def get_retry(
    url: str,
    sleep_time: float = 0.5,
    backoff_factor: float = 3.0,
    max_retries: int = 20,
    retry_status: list[int] | None = None,
    **kwargs,
) -> requests.Response | None:
    """
    Perform a get request, retrying with an exponential backoff when we
    encounter retryable errors. All parameters other than those listed below
    will be passed directly on to the ``requests.get`` call.

    :param url: The URL to fetch.
    :param sleep_time: The minimum time to sleep after any request, even
        successful.
    :param backoff_factor: After each retry, sleep for a further 2**attempt
        seconds, scaled by this backoff factor.
    :param max_retries: The maximum number of retries that we'll attempt.
    :param retry_status: Retry connections if we get a status code from this
        list.
    :return: The response, if successful, or None, if not.
    """
    # Set a reasonable default set of status codes if not specified
    if retry_status is None:
        retry_status = [429, 500, 502, 503, 504]

    for attempt in range(max_retries):
        try:
            response = requests.get(url, timeout=30, **kwargs)
            if response.status_code in retry_status:
                sleep(backoff_factor * (2**attempt))
                continue

            sleep(sleep_time)
            return response
        except requests.exceptions.ReadTimeout:
            # This is usually a temporary timeout, keep retrying with backoff
            continue
        except requests.exceptions.ConnectionError:
            pass
    return None


def get_parse_retry(
    url: str,
    parser: Callable[[str], Any | None],
    sleep_time: float = 0.5,
    backoff_factor: float = 3.0,
    max_retries: int = 10,
    **kwargs,
) -> Any | None:
    """
    Try downloading and parsing a resource found at an external URL, retrying
    both in the event of a failed get request and in the event of a failed
    parse.

    All parameters other than those listed below will be passed directly on to
    the ``requests.get`` call.

    :param url: The URL to fetch.
    :param parser: A function that parses the response into an object, or
        returns ``None`` on failure.
    :param sleep_time: The minimum time to sleep after any request, even
        successful.
    :param backoff_factor: After each retry, sleep for a further 2**attempt
        seconds, scaled by this backoff factor.
    :param max_retries: The maximum number of retries that we'll attempt.
    :return: The parsed object returned by the parser, or ``None`` on failure.
    """
    for attempt in range(max_retries):
        try:
            response = requests.get(url, timeout=30, **kwargs)

            # Back off and try again on any non-OK status code
            if response.status_code != HTTPStatus.OK:
                sleep(backoff_factor * (2**attempt))
                continue

            # We had a successful fetch, sleep the mandatory sleep time
            sleep(sleep_time)

            # Try to parse the result
            parsed = parser(response.text)
            if parsed is None:
                # The parse failed, so try to fetch again
                sleep(backoff_factor * (2**attempt))
                continue

            # Success: return the parsed object
            return parsed
        except:
            # Usually these are temporary read timeouts, or at worst connection
            # errors; keep retrying with backoff before we give up entirely
            continue

    # We used up all our retries, so give up
    return None
