# utils: Python utilities used in various scripts
# Copyright (C) 2023  Charles H. Pence
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
from typing import Any


def remove_nones(value: Any) -> Any:
    """
    Recursively remove all ``None`` values from dictionaries and lists, and
    returns the result as a new dictionary or list.

    This is most useful in sanitizing out ``None`` values before serializing to
    JSON, where those will turn into annoying ``null`` entries.

    :param value: The value to remove Nones from; usually a ``dict``
    :return: The object with all None values removed
    """
    if isinstance(value, list):
        return [remove_nones(x) for x in value if x is not None]
    elif isinstance(value, dict):
        return {
            key: remove_nones(val)
            for key, val in value.items()
            if val is not None
        }
    else:
        return value
