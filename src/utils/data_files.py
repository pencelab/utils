# utils: Python utilities used in various scripts
# Copyright (C) 2023  Charles H. Pence
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
import json
from datetime import datetime, timezone
from pathlib import Path
from shutil import move

from click import secho


def check_source(
    data: dict, source: str, version: int, key: str = "dataSources"
) -> bool:
    """
    Check to see whether a data source has already been added to this data.

    If this function returns ``True``, then the given source at the given
    version was already present.

    :param data: The loaded article metadata to check.
    :param source: The data source to check.
    :param version: The minimum version number to check.
    :param key: The kind of source to check, either ``dataSources`` or
        ``textSources``.

    :return: Returns whether the source is present.
    """
    if key in data:
        for s in data[key]:
            if s["source"] == source and s["version"] >= version:
                return True

    return False


def add_source(
    data: dict, source: str, version: int, key: str = "dataSources"
) -> dict:
    """
    Add a data source to this data object.

    :param data: The loaded article metadata to check.
    :param source: The data source to check.
    :param version: The minimum version number to check.
    :param key: The kind of source to check, either ``dataSources`` or
        ``textSources``.

    :return: Data updated with the given source information.
    """
    if key in data:
        for s in data[key]:
            if s["source"] == source:
                if s["version"] >= version:
                    secho(
                        f"WARNING: Asked to record version {version} of data source, but {s['version']} already present?",
                        fg="yellow",
                    )
                    return data

                # Update version number and return
                s["version"] = version

                ts = datetime.now(timezone.utc).replace(microsecond=0)
                s["timestamp"] = ts.isoformat()
                return data

    # Not present, add a new source
    if key not in data:
        data[key] = []

    ts = datetime.now(timezone.utc).replace(microsecond=0)
    new_source = {
        "source": source,
        "version": version,
        "timestamp": ts.isoformat(),
    }

    data[key].append(new_source)
    return data


def save_with_backup(data: dict, filename: str) -> None:
    """
    Save the given data to file, creating a backup file and blocking the save if
    a backup file already exists in the given path.

    :param data: The metadata object to save as JSON.
    :param filename: The filename at which to save the JSON.
    """
    # Move the current file to a backup, if it's there
    if Path(filename).exists():
        backup_path = Path(filename).with_suffix(".old")
        if backup_path.exists():
            secho(
                f"ERROR: Backup file ({backup_path}) already exists, aborting",
                fg="red",
            )
            return

        move(filename, backup_path)

    # Write out the result
    with open(filename, "w") as f:
        json.dump(data, f)
