# utils: Python utilities used in various scripts
# Copyright (C) 2023  Charles H. Pence
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
from os.path import exists, join

import platformdirs
import yaml


def path() -> str:
    """
    Return the path to the config file on the current platform.

    :return: The configuration file path to read.
    """
    config_dir = platformdirs.user_config_dir(
        appname="PenceLab", appauthor="PenceLab"
    )
    return join(config_dir, "pencelab.yml")


def load() -> dict[str, str | None]:
    """
    Load the configuration file, or a default skeleton of our configuration
    containing ``None`` values if the configuration cannot be loaded.

    :return: Configuration for our various utilities.
    """
    default_configuration: dict[str, str | None] = {"pubmed_api_key": None}

    filename = path()
    if exists(filename):
        with open(filename) as f:
            config = yaml.safe_load(f)

        ret = default_configuration.copy()
        ret.update(config)
        return ret

    return default_configuration.copy()
