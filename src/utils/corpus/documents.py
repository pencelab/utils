# utils: Python utilities used in various scripts
# Copyright (C) 2023  Charles H. Pence
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
import json

import click
from dateutil.parser import isoparse

from utils.decorators import compose

#: A set of options for a CLI application that will load a corpus
options = compose(
    click.option(
        "--field",
        "-f",
        type=click.Choice(
            ["citation", "filename", "doi", "authors", "title", "date", "year"],
            case_sensitive=False,
        ),
        help="The field to print when displaying documents.",
        default="citation",
        show_default=True,
    )
)


def format_document(filename: str, field: str = "citation") -> str:
    """
    Return a formatted representation of the given document. The user can select
    which fields of the document they would like to print using the provided
    click decorator.

    :param filename: The filename of the document to print.
    :param field: The field of the document to show. Can be set to citation \
    (for a formatted citation), filename, doi, authors, title, date, or year
    :return: A formatted representation of this field of the document.
    """
    if field == "filename":
        return filename

    with open(filename) as f:
        data = json.load(f)

        doi = data.get("doi") or ""
        title = data.get("title") or ""

        if field == "doi":
            return doi
        if field == "title":
            return title

        date = data.get("date") or ""
        dt = isoparse(date)

        if field in ("date", "year"):
            return str(dt.year)

        authors = []
        aus = data.get("authors")
        if aus is not None:
            for au in aus:
                authors.append(au.get("name"))
        authors = "; ".join(authors)

        if field == "authors":
            return authors

        return f"{authors} ({dt.year}). “{title}” doi: {doi}"
