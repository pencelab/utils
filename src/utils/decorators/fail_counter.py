# utils: Python utilities used in various scripts
# Copyright (C) 2023  Charles H. Pence
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
from typing import Any, Protocol


class FunctionWithFailCounter(Protocol):
    """
    This class wraps a function for use with the fail_counter decorator.

    :param fail_counter: The failure counter exposed to each function
    """

    fail_counter: int

    def __call__(self, *args, **kwargs):
        """
        Pass calls through to the function that we're going to wrap.
        """
        pass


def fail_counter(func: Any) -> FunctionWithFailCounter:
    """
    Decorator for functions that will give them a function-local failure counter
    attribute.

    To use this, decorate a function, then refer to the function's
    ``fail_counter`` attribute, making sure not to forget to initialize it in
    the global namespace after the function is defined:

    .. code-block:: python

       @fail_counter
       def my_function(params...):
           if my_function.fail_counter > TOO_MANY:
               print("oh no")

           if not do(things):
               my_function.fail_counter += 1

       my_function.fail_counter = 0

    :param func: The function to wrap
    :return: The decorated function
    """
    return func
